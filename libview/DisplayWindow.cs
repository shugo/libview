﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace libview
{
    public partial class DisplayWindow : Form
    {
        public static void Plot(IDisplayable Element)
        {
            DisplayWindow displayWindow = new DisplayWindow();
            Element.Draw(displayWindow.Graphics, new Rectangle(0, 0, 640, 480));
            System.Windows.Forms.Application.Run(displayWindow);
        }

        System.Drawing.Bitmap Image = new System.Drawing.Bitmap(640, 480);
        System.Drawing.Graphics _Graphics;

        public System.Drawing.Graphics Graphics
        {
            get { return _Graphics; }
        }   

        public DisplayWindow()
        {
            InitializeComponent();
            BackgroundImage = Image;
            BackgroundImageLayout = ImageLayout.Zoom;

            _Graphics = System.Drawing.Graphics.FromImage(Image);

        }

        private void DisplayWindow_Load(object sender, EventArgs e)
        {

        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pngToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void SaveImage_FileOk(object sender, CancelEventArgs e)
        {
            Image.Save(SaveImage.FileName);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveImage.ShowDialog();
        }
    }
}
