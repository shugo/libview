﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libview
{
    public class Graph : IDisplayable
    {
        List<Node> _nodes = new List<Node>();
        public class Node
        {
            float posx = 0.0f;
            float posy = 0.0f;
            float size = 1.0f;
            public float Size { get { return size; } }
            public float PositionX { get { return posx; } set { posx = value; } }
            public float PositionY { get { return posy; } set { posy = value; } }

            private System.Drawing.Brush _Brush = System.Drawing.Brushes.White;

            public System.Drawing.Brush Brush
            {
                get { return _Brush; }
                set { _Brush = value; }
            }


            List<Node> _Neighboor = new List<Node>();

            public List<Node> Neighboors
            {
                get { return _Neighboor; }
                set { _Neighboor = value; }
            }

        }

        public void AddNode(Node[,] Nodes)
        {
            int x = Nodes.GetLength(0);
            int y = Nodes.GetLength(1);

            for (int xx = 0; xx < x; xx++)
            {
                for (int yy = 0; yy < y; yy++)
                {
                    _nodes.Add(Nodes[xx, yy]);
                }
            }
        }

        public void AddNode(Node[] Nodes)
        {
            int x = Nodes.Length;

            for (int xx = 0; xx < x; xx++)
            {
                _nodes.Add(Nodes[xx]);
            }
        }

        public void AddNode(Node Node)
        {
            _nodes.Add(Node);
        }

        public void Draw(System.Drawing.Graphics graphics, System.Drawing.Rectangle Region)
        {
            float minx = float.MaxValue;
            float miny = float.MaxValue;
            float maxx = float.MinValue;
            float maxy = float.MinValue;

            for (int n = 0; n < _nodes.Count; n++)
            {
                if (_nodes[n].PositionX + _nodes[n].Size / 2 > maxx)
                    maxx = _nodes[n].PositionX + _nodes[n].Size / 2;
                if (_nodes[n].PositionX - _nodes[n].Size / 2 < minx)
                    minx = _nodes[n].PositionX - _nodes[n].Size / 2;
                if (_nodes[n].PositionY + _nodes[n].Size / 2 > maxy)
                    maxy = _nodes[n].PositionY + _nodes[n].Size / 2;
                if (_nodes[n].PositionY - _nodes[n].Size / 2 < miny)
                    miny = _nodes[n].PositionY - _nodes[n].Size / 2;


            }
            float scalefactorx = (float)(Region.Width - 1) / (maxx - minx);
            float scalefactory = (float)(Region.Height - 1) / (maxy - miny);

            foreach (Node _ in _nodes)
            {
                foreach (Node __ in _.Neighboors)
                    graphics.DrawLine(System.Drawing.Pens.Black,
                        (_.PositionX - minx) * scalefactorx + Region.Location.X,
                        (_.PositionY - miny)* scalefactory + Region.Location.Y,
                        (__.PositionX - minx) * scalefactorx + Region.Location.X,
                        (__.PositionY - miny) * scalefactory + Region.Location.Y);
            }


            foreach (Node _ in _nodes)
            {
                graphics.FillRectangle(System.Drawing.Brushes.White,
                    (_.PositionX - _.Size / 2 - minx) * scalefactorx + Region.Location.X,
                    (_.PositionY - _.Size / 2 - miny) * scalefactory + Region.Location.Y,
                    (_.Size * scalefactorx),
                    (_.Size * scalefactory));
                graphics.DrawRectangle(System.Drawing.Pens.Black,
                    (_.PositionX - _.Size / 2 - minx) * scalefactorx + Region.Location.X,
                    (_.PositionY - _.Size / 2 - miny) * scalefactory + Region.Location.Y,
                    (_.Size * scalefactorx),
                    (_.Size * scalefactory));
            }
        }
    }
}
