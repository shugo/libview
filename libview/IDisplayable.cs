﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libview
{
    public interface IDisplayable
    {
        void Draw(System.Drawing.Graphics graphics, System.Drawing.Rectangle Region);
    }
}
