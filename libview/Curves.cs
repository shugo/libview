﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libview
{
    public class Curves : IDisplayable
    {
        public class Curve
        {
            string _name = "";
            internal List<System.Drawing.PointF> _pair = new List<System.Drawing.PointF>();
            System.Drawing.Pen _pen = System.Drawing.Pens.Black;

            public System.Drawing.Pen Pen
            {
                get { return _pen; }
                set { _pen = value; }
            }

            public Curve(string Name)
            {
                _name = Name;
            }

            public void AddPoint(float x, float y)
            {
                System.Drawing.PointF p = new System.Drawing.PointF(x, y);
                for (int n = 0; n < _pair.Count; n++)
                {
                    if (_pair[n].X > x)
                    {
                        _pair.Insert(n, p);
                        return;
                    }
                }
                _pair.Add(p);
            }


        }

        List<Curve> _Curves = new List<Curve>();

        public void AddCurve(Curve Curve)
        {
            _Curves.Add(Curve);
        }

        public void Draw(System.Drawing.Graphics graphics, System.Drawing.Rectangle Region)
        {
            List<List<System.Drawing.Point>> Points = new List<List<System.Drawing.Point>>();

            float minx = float.MaxValue;
            float miny = float.MaxValue;
            float maxx = float.MinValue;
            float maxy = float.MinValue;

            List<System.Drawing.PointF> ticksX = new List<System.Drawing.PointF>();
            List<System.Drawing.PointF> ticksY = new List<System.Drawing.PointF>();


            for (int n = 0; n < _Curves.Count; n++)
            {
                List<System.Drawing.PointF> pts = new List<System.Drawing.PointF>(_Curves[n]._pair);


                for (int x = 0; x < pts.Count; x++)
                {
                    if (pts[x].X < minx)
                        minx = pts[x].X;
                    if (pts[x].Y < miny)
                        miny = pts[x].Y;

                    if (pts[x].X > maxx)
                        maxx = pts[x].X;
                    if (pts[x].Y > maxy)
                        maxy = pts[x].Y;

                    System.Drawing.PointF tickx = new System.Drawing.PointF(pts[x].X, 0.0f);
                    if (!ticksX.Contains(tickx)) { ticksX.Add(tickx); }

                    System.Drawing.PointF ticky = new System.Drawing.PointF(0.0f, pts[x].Y);
                    if (!ticksY.Contains(ticky)) { ticksY.Add(ticky); }
                }

                
            }


            float scalex = (Region.Width - 10) / maxx - minx;
            float scaley = (Region.Height - 10) / (maxy - 0);

            for (int n = 0; n < _Curves.Count; n++)
            {
                List<System.Drawing.PointF> pts = new List<System.Drawing.PointF>(_Curves[n]._pair);



                for (int x = 0; x < pts.Count; x++)
                {
                    pts[x] = new System.Drawing.PointF(5 + pts[x].X * scalex, Region.Height - pts[x].Y * scaley - 5);
                }

                graphics.DrawLines(_Curves[n].Pen, pts.ToArray());
            }

            graphics.DrawLine(System.Drawing.Pens.Black, 5, 0, 5, Region.Height - 1);
            graphics.DrawLine(System.Drawing.Pens.Black, 0, Region.Height - 1 - 5, Region.Width - 1, Region.Height - 1 - 5);

            for (int n = 0; n < ticksX.Count; n++)
            {
                graphics.DrawLine(System.Drawing.Pens.Black, ticksX[n].X * scalex + 5, Region.Height - 10, ticksX[n].X * scalex + 5, Region.Height);
            }
        }
    }
}
