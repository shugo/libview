﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libviewshell
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {

            string text = Console.ReadLine();
            libview.Graph Graph = new libview.Graph();
            libview.Curves Curves = new libview.Curves();
            Dictionary<string, libview.Graph.Node> Nodes = new Dictionary<string,libview.Graph.Node>();
            Dictionary<string, libview.Curves.Curve> SubCurve = new Dictionary<string,libview.Curves.Curve>();

            Dictionary<string, libview.IDisplayable> Figure = new Dictionary<string, libview.IDisplayable>();
            bool ModeGraph = false;
            bool ModeCurve = false;
            do
            {
                string[] tokens = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (tokens.Length == 1)
                {
                    if (tokens[0] == "}")
                    {
                        ModeGraph = false;
                        ModeCurve = false;
                    }
                    text = Console.ReadLine();
                    continue;
                }
                    if (ModeGraph)
                    {
                        if (tokens.Length == 4)
                        {
                            if (tokens[0] == "node")
                            {
                                float x = 0.0f, y = 0.0f;
                                string name = tokens[1];
                                float.TryParse(tokens[2], out x);
                                float.TryParse(tokens[3], out y);
                                libview.Graph.Node Node = new libview.Graph.Node();
                                Node.PositionX = x;
                                Node.PositionY = y;
                                Nodes.Add(name, Node);
                                Graph.AddNode(Node);
                            }

                        }
                        if (tokens.Length == 3)
                        {
                            if (tokens[1] == "-")
                            {
                                Nodes[tokens[0]].Neighboors.Add(Nodes[tokens[2]]);
                            }
                        }
                    }
                    else if (ModeCurve)
                    {
                        if (tokens.Length == 3)
                        {
                            if (tokens[0] == "curve")
                            {
                                string name = tokens[1];
                                string color = tokens[2];

                                System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.FromName(color));

                                libview.Curves.Curve Curve = new libview.Curves.Curve(name);
                                Curve.Pen = pen;
                                Curves.AddCurve(Curve);
                                SubCurve.Add(name, Curve);
                            }
                        }
                        if (tokens.Length == 4)
                        {
                           

                            if (tokens[0] == "point")
                            {
                                string curvename = tokens[1];
                                 float x = 0.0f, y = 0.0f;
                                float.TryParse(tokens[2], out x);
                                float.TryParse(tokens[3], out y);
                                SubCurve[curvename].AddPoint(x, y);

                            }

                        }
                    }
                    else
                    {

                        if (tokens.Length == 2)
                        {
                            if (tokens[0] == "plot")
                            {
                                libview.DisplayWindow.Plot(Figure[tokens[1]]);
                            }
                        }
                        if (tokens.Length == 3)
                        {
                            if (tokens[0] == "save")
                            {
                                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(512, 512);
                                System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bitmap);
                                Figure[tokens[1]].Draw(graphics, new System.Drawing.Rectangle(0, 0, 512, 512));
                                bitmap.Save(tokens[2]);
                            }
                            if (tokens[0] == "graph" && tokens[2] == "{")
                            {
                                string name = tokens[1];
                                Graph = new libview.Graph();
                                Nodes = new Dictionary<string, libview.Graph.Node>();
                                Figure.Add(name, Graph);
                                ModeGraph = true;
                            }
                            if (tokens[0] == "curves" && tokens[2] == "{")
                            {
                                string name = tokens[1];
                                Curves = new libview.Curves();
                                SubCurve = new Dictionary<string, libview.Curves.Curve>();
                                Figure.Add(name, Curves);
                                ModeCurve = true;
                            }
                        }
                    }
                    text = Console.ReadLine();
            } while (text != "end");
           
        }
    }
}
